package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;


import static org.mockito.Mockito.*;

public class UserDaoTests {
    private JdbcTemplate mockJdbcTemplate;
    private UserDAO userDAO;
    private final String thisNickname = "thisNickname";
    private final String thisEmail = "razdvatri@vteatrpri.hodi";
    private final String thisFullName = "Full Name";
    private final String thisAbout = "nothing to show. Ordinary man";


    @BeforeEach
    void prepare() {
        mockJdbcTemplate = Mockito.mock(JdbcTemplate.class);
        userDAO = new UserDAO(mockJdbcTemplate);
    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 1")
    void changeUserTest1() {
        UserDAO.Change(new User(thisNickname, null, null, null));
        Mockito.verifyNoInteractions(mockJdbcTemplate);

    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 2")
    void changeUserTest2() {
        UserDAO.Change(new User(thisNickname, thisEmail, thisFullName, thisAbout));
        verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));

    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 3")
    void changeUserTest3() {
        UserDAO.Change(new User(thisNickname, thisEmail, null, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));

    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 4")
    void changeUserTest4() {
        UserDAO.Change(new User(thisNickname, thisEmail, thisFullName, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));

    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 5")
    void changeUserTest5() {
        UserDAO.Change(new User(thisNickname, null, thisFullName, null));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 6")
    void changeUserTest6() {
        UserDAO.Change(new User(thisNickname, thisEmail, null, thisAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 7")
    void changeUserTest7() {
        UserDAO.Change(new User(thisNickname, null, null, thisAbout));
        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class));
    }

    @Test
    @DisplayName("MC/DC UserDAO change coverage test 8")
    void changeUserTest8() {
        UserDAO.Change(new User(thisNickname, null, thisFullName, thisAbout));

        Mockito.verify(mockJdbcTemplate).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(String.class), Mockito.any(String.class), Mockito.any(String.class));
    }
}
