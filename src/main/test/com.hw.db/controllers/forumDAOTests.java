package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class forumDAOTests {

    private JdbcTemplate mockJdbc;

// MC/DC
    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, null, false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #2")
    void ThreadListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #3")
    void ThreadListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #4")
    void ThreadListTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #5")
    void ThreadListTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }


    @Test
    @DisplayName("User gets list of threads test #6")
    void ThreadListTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

// Branch coverage

    @Test
    @DisplayName("User gets list of users test #1")
    void UserListTest1() {
        mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test #2")
    void UserListTest2() {
        mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",10, "12.02.2019", false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test #3")
    void UserListTest3() {
        mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug",null, null, false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
