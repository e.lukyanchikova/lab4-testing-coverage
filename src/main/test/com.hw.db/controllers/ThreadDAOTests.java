package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {

    private int id = 0;

    private boolean isDesc = true;
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void prepare() {
        mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }
    @Test
    @DisplayName("Tree sort statement test 1")
    void TreesortTest1() {
        ThreadDAO.treeSort(id,null, null, !isDesc);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Tree sort statement test 2")
    void TreesortTest2() {
        ThreadDAO.treeSort(id,null, null, isDesc);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch DESC ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }
}
