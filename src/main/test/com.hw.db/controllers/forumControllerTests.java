package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.Message;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class forumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @org.junit.jupiter.api.DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Not authorithed user fails to create forum")
    void noLoginWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some")).thenReturn(null);
            forumController controller = new forumController();
            try {
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Владелец форума не найден.")), controller.create(toCreate), "Result when unauthorised user tried to create a forum");}
            catch (NullPointerException Ex){
                fail("Null Pointer Exception was thrown");
            }
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("User fails to create forum because it already exists")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DuplicateKeyException("Forum already exists"));
                forumDAO.when(() -> ForumDAO.Info(toCreate.getSlug())).thenReturn(toCreate);

                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).body(toCreate), controller.create(toCreate), "Result for conflict while forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("User fails to create forum because server cannot access DB")
    void conflictWhenCreatesForumSecond() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate)).thenThrow(new DataAccessResourceFailureException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Что-то на сервере.")), controller.create(toCreate), "Result for DB error while server creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }


}