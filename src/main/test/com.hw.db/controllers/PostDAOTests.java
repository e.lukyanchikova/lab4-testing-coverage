package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PostDAOTests {
    Post firstPost;
    Post secondPost;
    private JdbcTemplate mockJdbc;
    String author1 = "author1";
    String author2 = "author2";
    String forum = "forum";
    String message1 = "message1";
    String message2 = "message2";
    int timeStamp1 = 1;
    int timeStamp2 = 2;

    @BeforeEach
    void prepare() {
        mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
        firstPost = new Post(author1, new Timestamp(timeStamp1), forum, message1, 0, 0, false);

    }


    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest1() {
        secondPost = new Post(author1, new Timestamp(timeStamp1), forum, message1, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("setPost basis path full coverage test 2")
    void setPostTest2() {
        secondPost = new Post(author1, new Timestamp(timeStamp1), forum, message2, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #3")
    void setPostTest3() {
        secondPost = new Post(author1, new Timestamp(timeStamp2), forum, message1, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest4() {
        secondPost = new Post(author2, new Timestamp(timeStamp1), forum, message1, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest5() {
        secondPost = new Post(author2, new Timestamp(timeStamp2), forum, message1, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest6() {
        secondPost = new Post(author1, new Timestamp(timeStamp2), forum, message2, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest7() {
        secondPost = new Post(author2, new Timestamp(timeStamp2), forum, message2, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("setPost basis path full coverage test #1")
    void setPostTest8() {
        secondPost = new Post(author2, new Timestamp(timeStamp1), forum, message2, 0, 0, false);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(firstPost);
        PostDAO.setPost(1, secondPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

}

